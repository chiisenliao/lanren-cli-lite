#!/usr/bin/env node

const program = require("commander")
const clc = require("cli-color")
const { inspect } = require("util")
const shell = require("shelljs")

const { errorColor, warnColor, successColor, normalColor } = require("./color/color")
const rsa = require("./rsa/rsa")

const package = require("./package.json")

program
  .version(clc.redBright("Version:" + package.version))
  .name(clc.greenBright("Name:" + package.name)) // 專案名稱
  .usage(clc.blueBright`-[命令參數] '副參數1' '副參數2' ...`) // 使用說明
  .addHelpText(
    "beforeAll",
    clc.yellow`
Example:
  $ lrl -h`
  )
  .description(clc.blueBright("Description: " + package.description))
  .option("-c | --rsa_create <dc>", successColor("產生指定 dc 的 RSA public/private key 檔案") + warnColor("(-c dc)"))
  .option("-v | --ver", normalColor("客製化的版本訊息")) // -V(大寫V) 預設為顯示版本號，小寫可使用
  .option("-z | --list", normalColor("顯示 npm 全域安裝的所有套件"))
  .showHelpAfterError(clc.red(`<使用 -h 參數可以提示更多使用功能>`)) // 錯誤提示訊息
  .configureOutput({
    // 此處使輸出變得容易區分
    writeOut: (str) => process.stdout.write(`[OUT] ${str}`),
    writeErr: (str) => process.stdout.write(`[ERR] ${str}`),
    // 將錯誤高亮顯示
    outputError: (str, write) => write(clc.redBright(str)),
  })
  .parse()

const opts = program.opts()
const keys = Object.keys(opts).length
if (keys <= 0) {
  console.log(clc.blue("請輸入參數，或輸入") + clc.red(" lrl -h ") + clc.blue("查詢使用說明。"))
  process.exit(1)
}

/**
 * 客製化的版本訊息
 */
if (opts.ver) {
  console.log(clc.blue("########################"))
  console.log(clc.blue("# ") + clc.magenta("專案名稱: ") + clc.red(package.name) + clc.blue(" #"))
  console.log(clc.blue("# ") + clc.magenta("目前版本: ") + clc.red(package.version) + clc.blue("      #"))
  console.log(clc.blue("########################"))
  console.log(clc.magenta("專案說明: ") + clc.red(package.description))
  console.log(clc.magenta("author: ") + clc.red(package.author))
  console.log(clc.magenta("repository: ") + clc.red(inspect(package.repository)))
  console.log(clc.magenta("license: ") + clc.red(package.license))
  console.log(clc.magenta("dependencies: ") + clc.red(inspect(package.dependencies)))
  console.log(clc.magenta("keywords: ") + clc.red(package.keywords))
}

/**
 *  產生指定 dc 的 RSA public/private key 檔案與 sql script
 */
if (opts.rsa_create) {
  rsa.exportKey(`【產生指定 dc 的 RSA KEY】`, program.getOptionValue("rsa_create"))
}

/**
 * 顯示 npm 全域安裝的所有套件
 */
if (opts.list) {
  shell.echo(clc.cyan("shell 執行開始!"))
  shell.echo(clc.red(`============================`))

  // 檢查指令是否安裝
  const cmd = "npm"
  if (!shell.which(cmd)) {
    shell.echo(`${errorColor("無法執行指令")}: ${warnColor(cmd)}`)
    shell.exit(1)
  }

  // 同步執行外部指令
  const extCmd = "npm list -g --depth=0"
  shell.echo(`執行 "${warnColor(extCmd)}"`)
  shell.echo(clc.blue(`============================`))
  const code = shell.exec(extCmd).code
  shell.echo(clc.blue(`============================`))
  shell.echo(`code= ${warnColor(code)}`)
  shell.echo(clc.red(`============================`))
  if (code !== 0) {
    shell.echo(`${errorColor("執行結果發生錯誤")}: ${warnColor(extCmd)}`)
    shell.exit(1)
  }
  shell.echo(clc.cyan("shell 執行結束!"))
}
