# lanren-cli-lite

lanren-cli 精簡版

![logo](https://img.my-best.tw/press_component/images/c14b4ca9124f4e97dda730eab61dfa43.jpg?ixlib=rails-4.2.0&q=70&lossless=0&w=690&fit=max)

[lanren-cli-lite : npm 說明頁面連結](https://www.npmjs.com/package/lanren-cli-lite)

# 安裝
安裝方式如下:
```
npm install -g lanren-cli-lite
```

---

# 查看版本號
```
lr -V
or
lr --version
```
大寫 -V 或 --version
![version](https://i.imgur.com/0sou4Hj.png)
```
lr -v
```
小寫 -v
![-v](https://i.imgur.com/4zHEXGx.png)

---

# 查看說明
```
lr -h
or
lr --help
```
![help](https://i.imgur.com/FRsCUGL.png)

---

# -c 產生指定 dc 的 RSA public/private key 檔案
```
lr -c dc
```
.\【產生指定 dc 的 RSA KEY】\dc\ 目錄內會有
.\YYYYMMDD\
        alter.sql
        README.md
.\dc\
    private.pem
    public.pem
這四個檔案
![RSA KEY](https://i.imgur.com/zPeJG8I.png)

---

# -z 顯示 npm 全域安裝的所有套件
```
lr  -z
```
![顯示 npm 全域安裝的所有套件](https://i.imgur.com/Mu0Ufs6.png)

---